<?php
/**
 * Vendor Project
 * Module Vendor/NavigationMenu
 *
 * @category  Vendor
 * @package   Vendor\NavigationMenu
 * @author    Your Name <your.name@email.com>
 * @copyright 2017 Vendor
 * @license   http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace BestResponseMedia\BootstrapMenu\Block\Html;

use Magento\Framework\Data\Tree\Node;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template;

/**
 * Plugin NavigationMenu
 *
 * @author    Your Name <your.name@email.com>
 * @copyright 2017 Vendor
 */
class Topmenu extends \Magento\Theme\Block\Html\Topmenu
{

    protected function _addSubMenu($child, $childLevel, $childrenWrapClass, $limit)
    {
        $html = '';
        if (!$child->hasChildren()) {
            return $html;
        }

        $colStops = null;
        if ($childLevel == 0 && $limit) {
            $colStops = $this->_columnBrake($child->getChildren(), $limit);
        }


        if ($childLevel == 0 && $child->hasChildren()) {
            $html .= '<ul class="list-unstyled row level' . $childLevel . ' ' . $childrenWrapClass . ' dropdown-menu"' . 'aria-labelledby=level' . $childLevel . '>';
        }
        /* If the subnavigation of level 1 is not needed on mobile views comment out this lines - Aquascutum requirement */
        else if ($childLevel != 0 && $child->hasChildren()) {
            $html .= '<ul class="list-unstyled hidden-custom row level' . $childLevel . ' ' . $childrenWrapClass . '"' . 'aria-labelledby=level' . $childLevel . '>';
        }
        /* If the subnavigation of level 1 is not needed on mobile views comment out this lines */
        else {
            $html .= '<ul class="list-unstyled level' . $childLevel . ' ' . $childrenWrapClass . '"' . '>';
        }

        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
        $html .= '</ul>';

        return $html;
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param Node $menuTree menu tree
     * @param string $childrenWrapClass children wrap class
     * @param int $limit limit
     * @param array $colBrakes column brakes
     * @return string
     *
     * @SuppressWarnings(PHPMD)
     */
    protected function _getHtml(
        Node $menuTree,
        $childrenWrapClass,
        $limit,
        $colBrakes = []
    )
    {

        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';


        foreach ($children as $child) {
            if ($childLevel === 0 && $child->getData('is_parent_active') === false) {
                continue;
            }
            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();


            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }
            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
                $html .= '</ul></li><li class="column"><ul>';
            }

            if ($childLevel == 2) {
                if ($counter % 6 == 1) {
                    $html .= '<div class="col-12 col-xl-6">';
                }
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . ' data-id="'. $child->getData('position_class') .'">';

            if ($childLevel == 0 && $child->hasChildren()) {
                $html .= '<a role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle" id="level' . $childLevel . '" href="' . $child->getUrl() . '"' . $outermostClassCode . '><span>' . $this->escapeHtml(
                        $child->getName()
                    ) . '</span></a>' . $this->_addSubMenu(
                        $child,
                        $childLevel,
                        $childrenWrapClass,
                        $limit
                    ) . '</li>';
            }

            /* If the subnavigation of level 1 is not needed on mobile views comment out this lines - Aquascutum requirement ussually not neeeded */
            else if ($childLevel != 0 && $child->hasChildren()) {
                $html .= '<a class="nav-link optional-expanding" href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                        $child->getName()
                    ) . '</span></a>' . $this->_addSubMenu(
                        $child,
                        $childLevel,
                        $childrenWrapClass,
                        $limit
                    ) . '</li>';
            }
            /* If the subnavigation of level 1 is not needed on mobile views comment out this lines end */

            else {
                $html .= '<a class="nav-link" href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                        $child->getName()
                    ) . '</span></a>' . $this->_addSubMenu(
                        $child,
                        $childLevel,
                        $childrenWrapClass,
                        $limit
                    ) . '</li>';
            }

            if ($childLevel == 2) {
                if ($counter % 6 == 0) {
                    $html .= '</div>';
                }
            }

            $itemPosition++;
            $counter++;
        }

        $transportObject = new DataObject(['html' => $html, 'menu_tree' => $menuTree]);
        $this->_eventManager->dispatch(
            'bestresponsemedia_topmenu_node_gethtml_after',
            ['menu' => $this->_menu, 'transport' => $transportObject]
        );
        $html = $transportObject->getHtml();

        return $html;
    }


    protected function _getMenuItemClasses(\Magento\Framework\Data\Tree\Node $item)
    {
        $classes = [];

        $classes[] = 'level' . $item->getLevel();
        $classes[] = $item->getPositionClass();

        if ($item->getIsFirst()) {
            $classes[] = 'first';
        }

        if ($item->getIsActive()) {
            $classes[] = 'active';
        } elseif ($item->getHasActive()) {
            $classes[] = 'has-active';
        }

        if ($item->getIsLast()) {
            $classes[] = 'last';
        }

        if ($item->getClass()) {
            $classes[] = $item->getClass();
        }

        if ($item->hasChildren() && $item->getLevel() != 1) {
            $classes[] = 'parent nav-item dropdown';
        }

        if (!$item->hasChildren()) {
            $classes[] = 'parent nav-item';
        }


        return $classes;
    }

}