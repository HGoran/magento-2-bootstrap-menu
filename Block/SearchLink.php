<?php

namespace BestresponseMedia\BootstrapMenu\Block;

class SearchLink extends \Magento\Framework\View\Element\Html\Link
{
    /**
     * Render block HTML.
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (false != $this->getTemplate())
        {
            return parent::_toHtml();
        }
        return '<li><a ' . $this->getLinkAttributes() . 'data-toggle="'. $this->getDataToggle() .'" data-target="'.$this->getDataTarget().'" >' . $this->escapeHtml($this->getLabel()) . '</a></li>';
    }
}