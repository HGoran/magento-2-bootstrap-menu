define([
    'jquery',
    'domReady!'
], function ($) {
    'use strict';

    $(function () {

        //Start mobile menu on click of the hamburger icon
        $('[data-toggle="offcanvas"]').on('click', function () {
            $('.offcanvas-collapse').toggleClass('open').removeClass('closed-menu');
        });

        //Close on a custom close button
        $('.close').click(function () {
            $('.offcanvas-collapse').addClass('closed-menu').toggleClass('open');
        });

        //Submenu expanding if needed can be uncommented for mobile if you do not want to use it. Please follow instructions in BootstrapMenu/Block/Html/Topmenu.php to remove the submenu expanding - will be later cleaned up and made as a config
        function subMenuExpanding() {
            if ($(window).width() < 1199) {
                $('.level0.dropdown-menu').css('display', '');
            }
            $('.optional-expanding').click(function () {
                var self = $(this);
                if ($(window).width() < 1199) {
                    if($(self).next().hasClass('hidden-custom')){
                        $(this).next().removeClass('hidden-custom');
                    } else {
                        $(this).next().addClass('hidden-custom');
                    }
                    return false;
                }
            });
        }

        subMenuExpanding();
        $(window).resize(function () {
            subMenuExpanding();
        });

        // If submenu items have images check and add some classes and reposition elements. Aquascutum requirement but to give general idea.
        $(".sub_category_image").each(function () {
            var id = $(this).attr('data-id');
            var self = $(this);
            var siblings = $(self).siblings();
            siblings.each(function () {
                var siblingSelf = $(this);
                if ($(siblingSelf).attr('data-id') == id) {
                    self.detach().prependTo($(siblingSelf).find('.nav-link'));
                    siblingSelf.parent().addClass('submenu-has-images');
                }
            });
        });




        //Hover & Click functionallity for top lvl menu items

        $('.nav-item.dropdown > a').click(function (e) {
            if($(window).width() >= 1200) {
                window.location.href = $(this).attr('href');
                return false;
            }
        });

        // On mouse enter we define the local variable of this
        $('.nav-item.dropdown > a').mouseenter(function(){
            var self = $(this);
            if($(window).width() >= 1200) {
                // This will fire on 1200 and above. First we reset everything with the line below
                $('.dropdown-menu').stop( true, true ).fadeOut().removeClass("show");
                // Then we fade in the apropriate menu. Previous step with reset was necessary to close and open diffrent dropdowns
                $(self).next('.dropdown-menu').stop( true, true ).fadeIn().addClass("show");
            }
        });

        // Body close of the menu
        $('body').mouseenter(function(){
            if($(window).width() >= 1200) {
                $('.dropdown-menu').stop(true, true).fadeOut().removeClass("show");
            }
        });


        // Dropdown menu close of the menu when we leave the dropdown menu area it will automatically close
        $(".navbar .dropdown .dropdown-menu").mouseleave(function () {
            var self = $(this);
            if($(window).width() >= 1200) {
                $(self ).stop( true, true ).fadeOut().removeClass("show");
            }
        });


    });

});